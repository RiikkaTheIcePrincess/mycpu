{-# LANGUAGE FlexibleContexts, BinaryLiterals #-}
module MYCPU.Instructions where
import Control.Concurrent (threadDelay) -- threadDelay µs
import Control.Lens hiding (cons)
import Control.Monad.State
import Data.Binary (encode, byteSwap16, decode)
import Data.Binary.Get
import Data.Bits
import Data.ByteString.Lazy as BSL (cons, append, drop, take)
import Data.Either
import Data.Maybe
import Data.Word (Word16, Word8, Word32)
import Data.Int  (Int16,  Int8, Int32)
import MYCPU.BitPatterns
import MYCPU.Misc
import MYCPU.SystemState

class Instruction a where
  interpret :: a -> YCFunc

instance Instruction ALUInstr where
  interpret inst = do
    state <- get
    let dest = inst ^. srcDstReg
    let ebm = inst ^. eightBitMode
    let eds = inst ^. extraSegment
    let op1 = state ^. registers . mapGP dest
    let op2pre = readOperand state eds (inst ^. opd2)
    case op2pre of
      Left irpt -> error "FIXME handle interrupts"
      Right op2 ->
        --Also maybe Maybe-ise the operands?
        --Cycle delay TODO FIXME etc.
        --lift $ threadDelay $ cycles (inst ^. opcode)*ceiling (1000*cycleDuration)
        case inst ^. opcode of
          CMP -> doCMP ebm op1 op2 state
          NEG -> doNEG ebm op2 dest state
          ADD -> doADD ebm op1 op2 dest state
          SUB -> doSUB ebm op1 op2 dest state
          ADC -> doADC ebm op1 op2 dest state
          SBC -> doSBC ebm op1 op2 dest state
          MUL -> doMUL ebm op1 op2 dest state
          DIV -> doDIV ebm op1 op2 dest state
          MLI -> doMLI ebm op1 op2 dest state
          DVI -> doDVI ebm op1 op2 dest state
          MOD -> doMOD ebm op1 op2 dest state
          MDI -> doMDI ebm op1 op2 dest state
          AND -> doAND ebm op1 op2 dest state
          ORR -> doORR ebm op1 op2 dest state
          EOR -> doEOR ebm op1 op2 dest state
          NOT -> doNOT ebm op2 dest state
          LOD -> doLOD ebm eds op2 dest state
          STO -> doSTO ebm eds op1 (readSOperand state eds (inst ^. opd2)) state

setFLFlags' n z c v = set' (registers . fl) fl'
  where
    fl' = n' .|. z' .|. c' .|. v'
    n' = if n then bit 0xF else 0
    z' = if z then bit 0xE else 0
    c' = if c then bit 0xD else 0
    v' = if v then bit 0xC else 0

setFLFlags :: Maybe Bool -> Maybe Bool -> Maybe Bool -> Maybe Bool -> SysState -> SysState
setFLFlags n z c v state = setFLFlags' n' z' c' v' state
  where
    n' = fromMaybe (getN state) n
    z' = fromMaybe (getZ state) z
    c' = fromMaybe (getC state) c
    v' = fromMaybe (getV state) v

getN state = testBit (state ^. registers . fl) 0xF
getZ state = testBit (state ^. registers . fl) 0xE
getC state = testBit (state ^. registers . fl) 0xD
getV state = testBit (state ^. registers . fl) 0xC
    
doCMP ebm op1 op2 state
  | ebm =  do
      let op1' = fromIntegral op1 :: Word8
      let op2' = fromIntegral op2 :: Word8
      let n = w16ToInt8 op1 >= w16ToInt8 op2
      let z = op1' == op2'
      let c = op1' >= op2'
      put $ setFLFlags (Just n) (Just z) (Just c) Nothing state
  | otherwise = do --16-bit mode
      let n = w16ToInt16 op1 >= w16ToInt16 op2
      let z = op1 == op2
      let c = op1 >= op2
      put $ setFLFlags (Just n) (Just z) (Just c) Nothing state

--0x8000 == 0b1000000000000000
--0x80 == 0b10000000
--These are max negative value and cannot be negated in twos complement
doNEG ebm op1 dest state
  | ebm =
    if (op1 .&. 0xFF) == 0x80 then --Can't negate this value (unsigned only)
      let state' = setFLFlags (Just True) (Just False) Nothing (Just True) state
      in put (set' (registers . mapGP dest) 0x80 state')
      else --8-bit, not 0x80/0b10000000
        let op1' = negate $ w16ToInt8 op1
            n = op1' < 0--testBit op1 0x7
            z = op1' == 0
            state' = setFLFlags (Just n) (Just z) Nothing (Just False) state
        in put (set'
                (registers . mapGP dest)
                (int8ToW16 op1')
                state')
  | otherwise =
    if op1 == 0x8000 then --Can't negate this either
      let state' = setFLFlags (Just True) (Just False) Nothing (Just True) state
      in put (set' (registers . mapGP dest) 0x8000 state')
      else --16-bit, not 0x8000
        let op1' = negate $ w16ToInt16 op1
            n = op1' < 0
            z = op1' == 0
            state' = setFLFlags (Just n) (Just z) Nothing (Just False) state
        in put (set'
                (registers . mapGP dest)
                (int16ToW16 op1')
                state')

addSub :: Bool -> Bool -> Word16 -> Word16 -> GPRegister -> SysState -> YCFunc
addSub add ebm op1 op2 dest state
  | ebm =
    let op1U = w16ToW8 op1
        op1S = w16ToInt8 op1
        op2U = w16ToW8 op2
        op2S = w16ToInt8 op2
        sumU = (if add then (+) else (-)) op1U op2U
        bigSumS = (if add then (+) else (-))
                  (fromIntegral op1S) (fromIntegral op2S :: Int32)
        bigSumU = (if add then (+) else (-))
                  (w8ToW32 op1U) (w8ToW32 op2U :: Word32)
        n = testBit sumU 0x7
        z = sumU == 0
        c = bigSumU > 0xFF --Outside range of Word8
        v = bigSumS < (-0x80) || bigSumS >= 0x80
        state' = setFLFlags (Just n) (Just z) (Just c) (Just v) state
    in put (set' (registers . mapGP dest) (w8ToW16 sumU) state')
  | otherwise =
    let op1S = w16ToInt16 op1
        op2S = w16ToInt16 op2
        sumU = (if add then (+) else (-)) op1 op2
        bigSumS = (if add then (+) else (-))
                  (fromIntegral op1S) (fromIntegral op2S :: Int32)
        bigSumU = (if add then (+) else (-))
                  (w16ToW32 op1) (w16ToW32 op2 :: Word32)
        n = testBit sumU 0xF
        z = sumU == 0
        c = bigSumU > 0xFFFF --Outside range of Word16
        v = bigSumS < (-0x8000) || bigSumS >= 0x8000
        state' = setFLFlags (Just n) (Just z) (Just c) (Just v) state
    in put (set' (registers . mapGP dest) sumU state')

doADD = addSub True
doSUB = addSub False
doADC ebm op1 op2 dest state = addSub True ebm op1 (op2 + c') dest state
  where
    c = testBit (state ^. registers . fl) 0xD
    c' = if c then 1 else 0
--Value is op1 - op2 - (1-C), so we say op1 - (op2+c').
--op1 - op2 - (1-1) == op1 - op2 - 0 == op1 - (op2+0)
--op1 - op2 - (1-0) == op1 - op2 - 1 == op1 - (op2+1) 
doSBC ebm op1 op2 dest state = addSub False ebm op1 (op2 + c') dest state
  where
    c = testBit (state ^. registers . fl) 0xD
    c' = if c then 0 else 1

doMUL ebm op1 op2 dest state
  | ebm =
    let lil1 = op1 .&. 0x00FF
        lil2 = op2 .&. 0x00FF
        lilProd = lil1 * lil2
        hiByte = lilProd .&. 0xFF00 --Get high byte
        loByte = lilProd .&. 0x00FF --Get low byte
        n = False
        z = lilProd == 0
        c = hiByte /= 0
        state' = setFLFlags (Just n) (Just z) (Just c) Nothing state
        state'' = set' (registers . mapGP R0) hiByte state'
    in put (set' (registers . mapGP dest) loByte state'')
  | otherwise =
    let prod = (fromIntegral op1) * (fromIntegral op2 :: Word32)
        --These will trim out the high and low words we need
        hiWord = w32ToW16hi prod
        loWord = w32ToW16lo prod
        n = False
        z = prod == 0
        c = hiWord /= 0
        state' = setFLFlags (Just n) (Just z) (Just c) Nothing state
        state'' = set' (registers . mapGP R0) hiWord state'
    in put (set' (registers . mapGP dest) loWord state'')

--TODO DivZeroFault stuff
--Figure out how to handle interrupts >.<
doDIV ebm op1 op2 dest state
  | ebm =
    let lil1 = op1 .&. 0x00FF
        lil2 = op2 .&. 0x00FF
        lilQuot = lil1 `div` lil2
        n = False
        z = lilQuot == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) lilQuot state')
  | otherwise =
    let quot = op1 `div` op2
        n = False
        z = quot == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) quot state')

doMLI ebm op1 op2 dest state
  | ebm =
    let lil1 = fromIntegral (w8ToInt8 $ w16ToW8 op1) :: Int16
        lil2 = fromIntegral (w8ToInt8 $ w16ToW8 op2) :: Int16
        prod = lil1 * lil2
        loByte = runGet getInt8 $ BSL.drop 1 $ encode prod
        hiByte = runGet getInt8 $ BSL.take 1 $ encode prod
        n = testBit hiByte 0x7
        z = prod == 0
        c = hiByte /= 0
        state' = setFLFlags (Just n) (Just z) (Just c) Nothing state
        state'' = set' (registers . mapGP R0) (int8ToW16 hiByte) state'
    in put (set' (registers . mapGP dest) (int8ToW16 loByte) state'')
  | otherwise =
    let lil1 = w16ToInt32 op1 :: Int32
        lil2 = w16ToInt32 op2 :: Int32
        prod = lil1 * lil2
        loWord = runGet getInt16be $ BSL.drop 2 $ encode prod
        hiWord = runGet getInt16be $ BSL.take 2 $ encode prod
        n = testBit hiWord 0xF
        z = prod == 0
        c = hiWord /= 0
        state' = setFLFlags (Just n) (Just z) (Just c) Nothing state
        state'' = set' (registers . mapGP R0) (int16ToW16 hiWord) state'
    in put (set' (registers . mapGP dest) (int16ToW16 loWord) state'')

doDVI ebm op1 op2 dest state
  | ebm =
    let lil1 = w8ToInt8 $ w16ToW8 op1 :: Int8
        lil2 = w8ToInt8 $ w16ToW8 op2 :: Int8
        res = lil1 `div` lil2
        n = False
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) (int8ToW16 res) state')
  | otherwise =
    let lil1 = w16ToInt16 op1 :: Int16
        lil2 = w16ToInt16 op2 :: Int16
        res = lil1 `div` lil2
        n = False
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) (int16ToW16 res) state')

doMOD ebm op1 op2 dest state
  | ebm =
    let lil1 = op1 .&. 0x00FF
        lil2 = op2 .&. 0x00FF
        lilMod = lil1 `mod` lil2
        n = testBit lilMod 0x7
        z = lilMod == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) lilMod state')
  | otherwise =
    let bigMod = op1 `mod` op2
        n = testBit bigMod 0xF
        z = bigMod == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) bigMod state')

doMDI ebm op1 op2 dest state
  | ebm =
    let lil1 = w8ToInt8 $ w16ToW8 op1 :: Int8
        lil2 = w8ToInt8 $ w16ToW8 op2 :: Int8
        res = lil1 `mod` lil2
        n = testBit res 0x7
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) (int8ToW16 res) state')
  | otherwise =
    let lil1 = w16ToInt16 op1 :: Int16
        lil2 = w16ToInt16 op2 :: Int16
        res = lil1 `mod` lil2
        n = testBit res 0xF
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) (int16ToW16 res) state')

doAND ebm op1 op2 dest state
  | ebm =
    let lil1 = w16ToW8 op1
        lil2 = w16ToW8 op2
        res = lil1 .&. lil2
        n = testBit res 0x7
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) (w8ToW16 res) state')
  | otherwise =
    let res = op1 .&. op2
        n = testBit res 0xF
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) res state')

doORR ebm op1 op2 dest state
  | ebm =
    let lil1 = w16ToW8 op1
        lil2 = w16ToW8 op2
        res = lil1 .|. lil2
        n = testBit res 0x7
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) (w8ToW16 res) state')
  | otherwise =
    let res = op1 .|. op2
        n = testBit res 0xF
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) res state')

doEOR ebm op1 op2 dest state
  | ebm =
    let lil1 = w16ToW8 op1
        lil2 = w16ToW8 op2
        res = lil1 `xor` lil2
        n = testBit res 0x7
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) (w8ToW16 res) state')
  | otherwise =
    let res = op1 `xor` op2
        n = testBit res 0xF
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) res state')

doNOT ebm op1 dest state
  | ebm =
    let lil1 = w16ToW8 op1
        res = complement lil1
        n = testBit res 0x7
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) (w8ToW16 res) state')
  | otherwise =
    let res = complement op1
        n = testBit res 0xF
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) res state')

doLOD :: Bool -> Bool -> Word16 -> GPRegister -> SysState -> YCFunc
doLOD ebm eds memDest dest state =
  case readMem' (if eds then ESU else DSU) ebm state memDest of
    Left irpt -> error "FIXME handle interrupts"
    Right val -> doLOD' ebm val dest state
doLOD' ebm memVal dest state
  | ebm =
    let lil1 = memVal -- .&. 0x00FF
        res = lil1
        n = testBit res 0x7
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) res state')
  | otherwise =
    let res = memVal
        n = testBit res 0xF
        z = res == 0
        state' = setFLFlags (Just n) (Just z) Nothing Nothing state
    in put (set' (registers . mapGP dest) res state')

doSTO :: Bool -> Bool -> Word16 -> STOOperand -> SysState -> YCFunc
--doSTO ebm eds op1 dest state
doSTO ebm eds op1 (PCR r) state = put (state & (registers . (mapPC r)) .~ op1)
doSTO ebm eds op1 (STOVal memDest) state =
  case writeMem' (if eds then ESU else DSU) ebm state memDest op1 of
    Left irpt -> error "FIXME TODO handle interrupts"
    Right func -> func --YCFunc implementing the STO effect

instance Instruction BRAInstr where
  interpret inst = do
    state <- get
    let delta = inst ^. offset
    let pcs = w16ToInt16 (state ^. registers . pc)
    let pc' = int16ToW16 (pcs + (fromIntegral delta :: Int16))
    let n = testFLN state
    let z = testFLZ state
    let c = testFLC state
    let v = testFLV state
    --Cycle delay TODO FIXME etc.
    --lift $ threadDelay $ cycles (inst ^. opcode)*ceiling (1000*cycleDuration)
    let cond = case inst ^. opcode of
                 BCCBUF -> not(c)
                 BCSBUH -> c
                 BNE -> not(z)
                 BEQ -> z
                 BPLBSF -> not(n)
                 BMIBSH -> n
                 BVC -> not(v)
                 BVS -> v
                 BUG -> not(z) && c
                 BSG -> not(z) && n
                 BAW -> True
    let state' = if cond then state & registers . pc .~ pc'
                         else state
    put state'

instance Instruction SHFInstr where
  interpret inst = do
    state <- get
    let dest = inst ^. register
    let dist = fromIntegral (inst ^. shiftVal) :: Int
    let val = state ^. registers . mapGP dest
    case inst ^. opcode of
      ASL -> doASL state dest val dist
      ASR -> doASR state dest val dist
      LSL -> doASL state dest val dist --Equivalent to ASL
      LSR -> doLSR state dest val dist
      ROL -> doROL state dest val dist
      ROR -> doROR state dest val dist
      RNL -> doRNL state dest val dist
      RNR -> doRNR state dest val dist

doASL state dest val dist =
  let bigVal = w16ToW32 val
      bigVal' = shiftL bigVal dist
      val' = w32ToW16lo bigVal'-- .&. 0xFFFF
      n = testBit val' 0xF
      z = val' == 0
      c = bigVal' .&. 0xFFFF0000 > 0
      state' = setFLFlags (Just n) (Just z) (Just c) Nothing state
  in put (set' (registers . mapGP dest) val' state')

doASR state dest val dist =
  let preVal' = shiftR (w16ToInt16 val) (fromIntegral dist)
      val' = int16ToW16 preVal'
      n = testBit val' 0xF
      z = val' == 0
      c = popCount val > popCount val'
      state' = setFLFlags (Just n) (Just z) (Just c) Nothing state
  in put (set' (registers . mapGP dest) val' state')

doLSR state dest val dist =
  let val' = shiftR val (fromIntegral dist)
      n = testBit val' 0xF
      z = val' == 0
      c = popCount val > popCount val'
      state' = setFLFlags (Just n) (Just z) (Just c) Nothing state
  in put (set' (registers . mapGP dest) val' state')

doROL state dest val dist =
  let (val',c') = roll (testFLC state) val dist
      n = testBit val' 0xF
      z = val' == 0
      state' = setFLFlags (Just n) (Just z) (Just c') Nothing state
  in put (set' (registers . mapGP dest) val' state')
roll c v n
  | n > 0 =
    let c' = testBit v 0xF
        v' = rotateL (if c then setBit v 0xF else clearBit v 0xF) 1
    in roll c' v' (n-1)
  | n == 0 = (v,c)

doROR state dest val dist =
  let (val',c') = rock (testFLC state) val dist
      n = testBit val' 0xF
      z = val' == 0
      state' = setFLFlags (Just n) (Just z) (Just c') Nothing state
  in put (set' (registers . mapGP dest) val' state')
rock c v n
  | n > 0 =
    let c' = testBit v 0
        v' = rotateR (if c then setBit v 0 else clearBit v 0) 1
    in rock c' v' (n-1)
  | n == 0 = (v,c)

doRNL state dest val dist =
  let val' = rotateL val dist
      n = testBit val' 0xF
      z = val' == 0
      state' = setFLFlags (Just n) (Just z) Nothing Nothing state
  in put (set' (registers . mapGP dest) val' state')
      
doRNR state dest val dist =
  let val' = rotateR val dist
      n = testBit val' 0xF
      z = val' == 0
      state' = setFLFlags (Just n) (Just z) Nothing Nothing state
  in put (set' (registers . mapGP dest) val' state')

instance Instruction BTIInstr where
  interpret inst = do
    state <- get
    let dest = inst ^. register
    let tgt = inst ^. bitToTest
    let val = state ^. registers . mapGP dest
    case inst ^. opcode of
      BTT -> doBTT state val tgt
      BTX -> doBTX state val tgt dest
      BTC -> doBTC state val tgt dest
      BTS -> doBTS state val tgt dest

doBTT state val tgt = put $ setFLFlags Nothing (Just z) Nothing Nothing state
  where z = not(testBit val (fromIntegral tgt))

doBTX state val tgt dest =
  let val' = complementBit val (fromIntegral tgt)
      --Z if val has target bit clear
      z = not(testBit val (fromIntegral tgt))
      --C if val has target bit set
      c = not(z)
      state' = setFLFlags Nothing (Just z) (Just c) Nothing state
  in put (set' (registers . mapGP dest) val' state')

doBTC state val tgt dest =
  let val' = clearBit val (fromIntegral tgt)
      --Z if val has target bit clear
      z = not(testBit val (fromIntegral tgt))
      --C if bit changed
      c = val /= val'
      state' = setFLFlags Nothing (Just z) (Just c) Nothing state
  in put (set' (registers . mapGP dest) val' state')

doBTS state val tgt dest =
  let val' = setBit val (fromIntegral tgt)
      --Z if val has target bit clear
      z = not(testBit val (fromIntegral tgt))
      --C if bit changed
      c = val /= val'
      state' = setFLFlags Nothing (Just z) (Just c) Nothing state
  in put (set' (registers . mapGP dest) val' state')

instance Instruction SEIInstr where
  interpret inst = do
    state <- get
    let dest = inst ^. dstRegister
    let nval = inst ^. newValue
    --There's only actually one opcode (SET) *shrug*
    put (set' (registers . mapGP dest) nval state)

instance Instruction FLGInstr where
  interpret inst = do
    state <- get
    let n' = inst ^. n
    let z' = inst ^. z
    let c' = inst ^. c
    let v' = inst ^. v
    case inst ^. opcode of
      SEF -> modFL True n' z' c' v' state
      CLF -> modFL False n' z' c' v' state

modFL set n z c v state = put $ setFLFlags (check n) (check z) (check c) (check v) state
  where check b = if b then (Just set) else Nothing

instance Instruction STKInstr where
  interpret inst = do
    state <- get
    let op = inst ^. opcode
    let reg = inst ^. register
    case op of
      PSH -> doPSH state reg
      POP -> doPOP state reg

--FIXME
doPSH state (Left gpr) =
  let val = state ^. registers . mapGP gpr
      sp = state ^. registers . if isSModeEnabled state then ssp else usp
  in put state
  
doPOP state (Left gpr) =
  let val = state ^. registers . mapGP gpr
      sp = state ^. registers . if isSModeEnabled state then ssp else usp
  in put state

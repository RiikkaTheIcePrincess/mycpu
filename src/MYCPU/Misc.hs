module MYCPU.Misc where
import Control.Lens hiding (cons)
import Control.Monad.State
import Data.Bits
import Data.Word
import Data.Int
import Data.ByteString.Lazy as BSL (cons, append, drop, take)
import Data.Binary (encode, byteSwap16, decode)
import Data.Binary.Get
--import Data.Vector hiding (cons)
import Data.Sequence as Sq hiding (cons)
import Data.Maybe
import MYCPU.SystemState

mkFields = makeLensesWith classUnderscoreNoPrefixFields
--Constants and infrastructure types and stuff?
--Instantiation functions are intended to receive end values
--  For example, `fiveBitsPlusOne` does not take the raw five-bit value but the
--  incremented value (on [1,32] rather than [0,31])

--Duration of a CPU cycle in milliseconds
cycleDuration = 12.47

data ALUAddressing =
  Addr JAddressing
  | ControlRegister PCRegister
  deriving Show
data JAddressing =
  Immediate Word16
  | Absolute Word16
  | Register GPRegister
  | Indirect GPRegister
  | IndirectOffset GPRegister Word16
  | IndirectIndexed GPRegister IxRegister
  deriving Show

--A,B,C,D,W,X,Y,Z
data GPRegister = R0 | R1 | R2 | R3 | R4 | R5 | R6 | R7 deriving (Show,Eq)
-- Remember that these are aliases for R4,R5,R6,R7
data IxRegister = W | X | Y | Z deriving Show
data PCRegister = FL | PC | PS | USP | SSP deriving Show
data SegRegister = CSU | DSU | ESU | SSU
  | CSS | DSS | ESS | SSS | IS deriving Show

mapXR W = r4
mapXR X = r5
mapXR Y = r6
mapXR Z = r7

mapGP R0 = r0
mapGP R1 = r1
mapGP R2 = r2
mapGP R3 = r3
mapGP R4 = r4
mapGP R5 = r5
mapGP R6 = r6
mapGP R7 = r7

mapPC FL = fl
mapPC PC = pc
mapPC PS = ps
mapPC USP = usp
mapPC SSP = ssp

mapSeg CSU = csu
mapSeg DSU = dsu
mapSeg ESU = esu
mapSeg SSU = ssu
mapSeg CSS = css
mapSeg DSS = dss
mapSeg ESS = ess
mapSeg SSS = sss
mapSeg IS  = is

getFL state = state ^. registers . fl
--Test N flag in FL
testFLN state = testBit (getFL state) 0xF
testFLZ state = testBit (getFL state) 0xE
testFLC state = testBit (getFL state) 0xD
testFLV state = testBit (getFL state) 0xC

readOperand state eds (ControlRegister pcr) = Right $ state ^. registers . mapPC pcr
readOperand state eds (Addr (Immediate w)) = Right w
readOperand state eds (Addr (Register r)) = Right $ state ^. registers . mapGP r
readOperand state eds opd = readMem' seg False state (readOpValue state eds opd)
  where seg = if eds then ESU else DSU

--Addressing is NOT indirect, by which I mean:
--"Absolute 1" means STO to memory address 0x0001
data STOOperand = PCR PCRegister | STOVal Word16 deriving Show
readSOperand state eds (ControlRegister pcr) = PCR pcr
readSOperand state eds (Addr (Register _)) =
  error "Register addressing not supported for STO" --TODO handle interrupts
readSOperand state eds (Addr (Immediate _)) =
  error "Immediate addressing not supported for STO" --TODO handle interrupts
readSOperand state eds a = STOVal $ readOpValue state eds a

--Cheeky/cheaty I guess but reduces duplication?
readOpValue state eds (Addr (Immediate w)) =
  error "readOpValue is for memory addressing"
readOpValue state eds (Addr (Register r)) =
  error "readOpValue is for memory addressing"
readOpValue state eds (Addr (Absolute addr)) =
  fromIntegral addr
readOpValue state eds (Addr (Indirect r)) =
  fromIntegral (state ^. registers . mapGP r)
readOpValue state eds (Addr (IndirectOffset r w)) =
  fromIntegral (w + (state ^. registers . mapGP r))
readOpValue state eds (Addr (IndirectIndexed r x)) =
  fromIntegral (rv + xv)
  where
    rv = state ^. registers . mapGP r
    xv = state ^. registers . mapXR x

getCurrentSeg :: SysState -> SegRegister -> SegRegister
getCurrentSeg state reg =
  let psReg = state ^. registers . ps
      --We'll call it user mode when MMU is disabled
      sMode = testBit psReg 0xF .&. testBit psReg 0xE
  in case reg of
    CSU -> if sMode then CSS else CSU
    DSU -> if sMode then DSS else DSU
    ESU -> if sMode then ESS else ESU
    SSU -> if sMode then SSS else SSU
    IS  -> IS

-- -> (D,W,P,A,base address, size, Maybe device ID)
getSeg :: SysState -> SegRegister ->
  (Bool, Bool, Bool, Bool,
   Word32, Word32, Maybe Word8)
getSeg state reg =
  let seg = if isMMUEnabled state
        then state ^. registers . mapSeg reg
        else getNoMMUSeg reg
      d = testBit seg 31
      w = testBit seg 30
      p = testBit seg 29
      a = testBit seg 28
      baseAddr = flip shiftL 8 (if d then seg .&. 0x0000FFFF else seg .&. 0x000FFFFF)
      size = (flip shiftL 8 (seg .&. 0x0FF00000)) + 256
      preDevID = fromIntegral $ flip shiftR 16 $ seg .&. 0x000F0000
      deviceID = if d then Just preDevID else Nothing
  in (d,w,p,a,baseAddr,size,deviceID)

getNoMMUSeg :: SegRegister -> Word32
getNoMMUSeg CSU = 0x8FF00000
getNoMMUSeg DSU = 0x0FF00000
getNoMMUSeg ESU = 0x0FF00000
getNoMMUSeg SSU = 0x0FF00000
getNoMMUSeg IS  = 0x8FF00000

isMMUEnabled state = testBit psReg 0xE
  where psReg = state ^. registers . ps
isSModeEnabled state = testBit psReg 0xF
  where psReg = state ^. registers . ps

_getMem :: Seq Word8 -> Int -> Bool -> Word16
_getMem mem addr ebm =
  case mem !? addr of
    Nothing -> fromIntegral 0 --Access outside connected memory gives 0
    Just v -> if ebm then w8ToW16 v
               else runGet getWord16le (v `cons` encode nextV)
  where nextV = w16ToW8 $ _getMem mem (addr+1) True

readMem = readMem' DSU False
--TODO handle memory access times and alignment delay
--Accesses "come from" device may be wrong
--I think accesses "go to" or "read from" the device, at least in the case of 0 (ROM)
readMem' :: SegRegister -> Bool -> SysState -> Word16 -> Either Interrupt Word16
readMem' reg ebm state addr =
  if notConnected then Right 0
  else
    if tooFar then
      if mmu then Left SegFault
      --Not possible because all 16b addresses are available with MMU disabled
      else error "Access outside non-MMU bound?!?"
    else if mmu && not(p) then Left SegFault
         else Right $ _getMem mem addr' ebm
  where
    (d,w,p,a,base,size,devID) =
        getSeg state (getCurrentSeg state reg)
    tooFar = w16ToW32 addr >= size
    notConnected = base + (fromIntegral addr) > (fromIntegral $ Sq.length mem)
    mmu = isMMUEnabled state
    addr' = (fromIntegral addr) + fromIntegral base
    mem = if not d then state ^. ramemory
                      else case devID of
                             Just 0 -> state ^. romemory
                             --TODO device access
                             Just n -> state ^. ioPorts

--Access to non-existent memory will silently fail
--Per spec (3.F.5) this is correct
_setMem :: Seq Word8 -> Int -> Bool -> Word16 -> Seq Word8
_setMem mem addr ebm val
  | ebm = mem'
  | otherwise = _setMem mem' (addr+1) True (byteSwap16 val)
  where mem' = mem & ix addr .~ (w16ToW8 val)

--TODO FIXME handle additional device memory access
writeMem = writeMem' DSU False
writeMem' :: SegRegister -> Bool -> SysState -> Word16 ->
            Word16 -> Either Interrupt YCFunc
writeMem' reg ebm state addr val =
  if notConnected then Right $ return () --No change, silent fail
  else
    if tooFar then
      if mmu then Left SegFault
      --Not possible because all 16b addresses are available with MMU disabled
      else error "Access outside non-MMU bound?!?"
    else if mmu && not(p) then Left SegFault
         else state' --Finally we can output!
  where
    (d,w,p,a,base,size,devID) =
        getSeg state (getCurrentSeg state reg)
    tooFar = w16ToW32 addr >= size
    notConnected = addr' >= (fromIntegral $ Sq.length mem)
    mmu = isMMUEnabled state
    addr' = (fromIntegral addr) + fromIntegral base
    state' = if not d then Right (put (set' ramemory (_setMem mem addr' ebm val) state))
             else case devID of
                       Just 0 -> Right (put (set' romemory (_setMem mem addr' ebm val) state))
                       Just n -> Right (put (set' ioPorts (_setMem mem addr' ebm val) state))
    mem = if not d then state ^. ramemory
                      else case devID of
                             Just 0 -> state ^. romemory
                             --TODO device access
                             Just n -> state ^. ioPorts

--Swap bytes because we want the low bits, not the high ones
w16ToInt8 :: Word16 -> Int8
w16ToInt8 w = runGet getInt8 $ encode $ byteSwap16 w
--TODO remember to convert to big-endian in parsing and
--use that internally for consistency or something
--TODO figure out how and why endianness is the most confusing thing in the world

--See w16ToW8: to get the low byte we have to swap bytes.
w16ToInt16 :: Word16 -> Int16
w16ToInt16 = fromIntegral
w16ToInt32 :: Word16 -> Int32
w16ToInt32 = fromIntegral . w16ToInt16
w16ToW8 :: Word16 -> Word8
w16ToW8 w = runGet getWord8 $ encode $ byteSwap16 w
w8ToW16 :: Word8 -> Word16
w8ToW16 w = runGet getWord16be ((0 :: Word8) `cons` encode w)
w8ToInt8 :: Word8 -> Int8
w8ToInt8 = fromIntegral
zo = encode (0 :: Word8)
w8ToW32 :: Word8 -> Word32
w8ToW32 w = runGet getWord32be $ zo `append` zo `append` zo `append` encode w
w16ToW32 :: Word16 -> Word32
w16ToW32 w = runGet getWord32be $ zo `append` zo `append` encode w
w32ToW16lo :: Word32 -> Word16
w32ToW16lo w = runGet getWord16be $ BSL.drop 2 $ encode w
w32ToW16hi :: Word32 -> Word16
w32ToW16hi w = runGet getWord16be $ BSL.take 2 $ encode w

int8ToW16 :: Int8 -> Word16
int8ToW16 w = runGet getWord16be ((0 :: Word8) `cons` encode w) :: Word16
int8ToW8 :: Int8 -> Word8
int8ToW8 w = runGet getWord8 $ encode w :: Word8
int16ToW16 :: Int16 -> Word16
int16ToW16 w = runGet getWord16be $ encode w
int32ToW16lo :: Int32 -> Word16
int32ToW16lo w = runGet getWord16be $ BSL.drop 2 $ encode w
int32ToW16hi :: Int32 -> Word16
int32ToW16hi w = runGet getWord16be $ BSL.take 2 $ encode w

--TODO handle PC increments somewhere
{-
--Implement as +2 PC per word read?
pcIncrement :: Addressing -> Word16
pcIncrement (Immediate _)      = 4
pcIncrement (Immediate Nothing)       = 8
pcIncrement (Absolute (Just _))       = 4
pcIncrement (Absolute Nothing)        = 4
pcIncrement  ControlRegister          = 2
pcIncrement  Register                 = 2
pcIncrement  Indirect                 = 2
pcIncrement (IndirectOffset (Just _)) = 2
pcIncrement (IndirectOffset Nothing)  = 4
pcIncrement (IndirectIndexed _)       = 2
-}

class Delayed a where
  cycles :: a -> Int

--Instructions using the ALU bit pattern
data ALUOp = CMP | NEG | ADD | SUB | ADC | SBC | MUL | DIV | MLI
  | DVI | MOD | MDI | AND | ORR | EOR | NOT | LOD | STO deriving Show
instance Delayed ALUOp where
  cycles CMP = 1
  cycles NEG = 1
  cycles ADD = 1
  cycles SUB = 1
  cycles ADC = 1
  cycles SBC = 1
  cycles MUL = 8
  cycles DIV = 48
  cycles MLI = 8
  cycles DVI = 48
  cycles MOD = 48
  cycles MDI = 48
  cycles AND = 1
  cycles ORR = 1
  cycles EOR = 1
  cycles NOT = 1
  cycles LOD = 1
  cycles STO = 1

--Instructions using the branch bit pattern
data BRAOp = BCCBUF | BCSBUH | BNE | BEQ | BPLBSF | BMIBSH
  | BVC | BVS | BUG | BSG | BAW deriving Show
instance Delayed BRAOp where
  cycles _ = 1

--Instructions using the bit testing bit pattern
data BTIOp = BTT | BTX | BTC | BTS deriving Show
instance Delayed BTIOp where
  cycles _ = 2

--Instructions using the flags bit pattern
data FLGOp = CLF | SEF deriving Show
instance Delayed FLGOp where
  cycles _ = 1

--Instructions using the hardware interface bit pattern
data HWIOp = HWQ deriving Show
data HWQuery = NumDevices | QueryDeviceR0 | Message |
  GetRamRom | GetRTCTime | GetRTCInterval | SetRTCInterval
instance Delayed HWIOp where
  cycles _ = 4

--Instructions using the increment/immediate bit pattern
data IMMOp = ADI | SBI deriving Show
--Only use `fiveBitsPlusOne` to instantiate
newtype FiveBitsPlusOne = UnsafeFBPO { fbpoval :: Int }
fiveBitsPlusOne :: Int -> FiveBitsPlusOne
fiveBitsPlusOne n
  | n >= 1 && n <= 32 = UnsafeFBPO n
  | otherwise = error "IMM or SHF value out of bounds"
instance Delayed IMMOp where
  cycles _ = 1

--Instructions using the jump bit pattern
data JMIOp = JMP | JSR deriving Show
--Only use `twentyBitsLS8` to instantiate
newtype TwentyBitsLS8 = Unsafe20BLS8 { tbls8val :: Int }
twentyBitsLS8 :: Int -> TwentyBitsLS8
twentyBitsLS8 n
  | n == 0 || (256 <= n && n <= 2^28-256) = Unsafe20BLS8 n
  | otherwise = error "Far jump segment base value out of bounds"
--Only use `eightBitsILS8` to instantiate
newtype EightBitsILS8 = UnsafeEBILS8 { ebils8val :: Int }
eightBitsILS8 :: Int -> EightBitsILS8
eightBitsILS8 n
  | 256 <= n && n <= 2^16 = UnsafeEBILS8 n
  | otherwise = error "Far jump segment size value out of bounds"
instance Delayed JMIOp where
  cycles JMP = 2
  cycles JSR = 4
  
--Instructions using the MMU bit pattern
data MMUOp = LSG | SSG deriving Show
instance Delayed MMUOp where
  cycles _ = 4

--Instructions using the processor functions bit pattern
data PRXOp = RTS | RTSF | RTI | SWI | SLP deriving Show
instance Delayed PRXOp where
  cycles _ = 2

--Instructions using the set immediate(?) bit pattern
data SEIOp = SET deriving Show
--Only use `fiveBits` to instantiate
newtype FiveBits = UnsafeFB { fbval :: Int }
fiveBits :: Int -> FiveBits
fiveBits n
  | 0 <= n && n <= 31 = UnsafeFB n
  | otherwise = error "SEI (SET) value out of bounds"
--Only use `weirdSEIVal` to instantiate
newtype WeirdSEIVal = UnsafeWSEIV { wseivval :: Int }
weirdSEIVal :: FiveBits -> WeirdSEIVal
weirdSEIVal (UnsafeFB n)
  | n <= 0x0A = UnsafeWSEIV (2^(5+n))
  | otherwise = UnsafeWSEIV (n+0xFFE0)
instance Delayed SEIOp where
  cycles _ = 1

--Instructions using the bit shifting bit pattern
data SHFOp = ASL | LSL | ROL | RNL | ASR | LSR | ROR | RNR deriving Show
instance Delayed SHFOp where
  cycles ASL = 4
  cycles ASR = 4
  cycles LSL = 4
  cycles LSR = 4
  cycles ROL = 2
  cycles ROR = 2
  cycles RNL = 2
  cycles RNR = 2

--Instructions using the stack immediate bit pattern
data STXOp = STX deriving Show
--Only use `stxOffset` to instantiate
newtype STXOffset = UnsafeSTXOffset { stxoval :: Int }
stxOffset :: Int -> STXOffset
stxOffset n
  | -256 <= n && n <= 254 && n `mod` 2 == 0 = UnsafeSTXOffset n
  | otherwise = error "STX offset value out of bounds"
instance Delayed STXOp where
  cycles _ = 1

--Instructions using the stack bit pattern
data STKOp = PSH | POP deriving Show
instance Delayed STKOp where
  cycles _ = 1

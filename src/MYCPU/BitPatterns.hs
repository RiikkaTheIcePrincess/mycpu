{-# LANGUAGE DuplicateRecordFields, TemplateHaskell,
    MultiParamTypeClasses, FlexibleInstances, FunctionalDependencies #-}
module MYCPU.BitPatterns where
import Control.Lens
import Data.Either
import Data.Word
import Data.Int
import MYCPU.Misc
import MYCPU.SystemState (SysState, RegState, YCFunc)
{-# ANN module "HLint: ignore Unused LANGUAGE pragma" #-}

data ALUInstr = ALUInstr {
  _opcode        :: ALUOp,
  _opd2          :: ALUAddressing,
  _eightBitMode  :: Bool,
  _extraSegment  :: Bool,
  _srcDstReg     :: GPRegister}
mkFields ''ALUInstr

data BRAInstr = BRAInstr { --[Joke] Not "brain struction"
  _opcode        :: BRAOp,
  _offset        :: Int8}
mkFields ''BRAInstr

data BTIInstr = BTIInstr {
  _opcode        :: BTIOp,
  _bitToTest     :: Word8,
  _register      :: GPRegister}
mkFields ''BTIInstr

data FLGInstr = FLGInstr {
  _opcode        :: FLGOp,
  _n,_z,_c,_v    :: Bool}
mkFields ''FLGInstr

data HWIInstr = HWIInstr {
  _opcode        :: HWIOp,
  _queryType     :: HWQuery}
mkFields ''HWIInstr

data IMMInstr = IMMInstr {
  _opcode        :: IMMOp,
  _incrValue     :: FiveBitsPlusOne,
  _register      :: GPRegister}
mkFields ''IMMInstr

data JMIInstr =  JMIInstr {
  _opcode        :: JMIOp,
  _opd1          :: Maybe Word16,
  _opd2          :: Maybe Word16,
  _farJump       :: Bool,
  _farInfo       :: FarJumpInfo,
  _register      :: GPRegister}
data FarJumpInfo = FarJumpInfo {
  _newPC          :: Word16,
  _newSegBaseAddr :: TwentyBitsLS8,
  _newSegSize     :: EightBitsILS8}
mkFields ''JMIInstr
mkFields ''FarJumpInfo

data MMUInstr = MMUInstr {
  _opcode        :: MMUOp,
  _register      :: SegRegister}
mkFields ''MMUInstr

--Shutup, hlint (data/newtype)
newtype PRXInstr = PRXInstr {
  _opcode        :: PRXOp}
mkFields ''PRXInstr

data SEIInstr = SEIInstr {
  _opcode        :: SEIOp,
  --We'll parse out a proper value at the low level
  _newValue      :: Word16,
  _dstRegister   :: GPRegister}
mkFields ''SEIInstr

data SHFInstr = SHFInstr {
  _opcode        :: SHFOp,
  _shiftVal      :: Word8,
  _register      :: GPRegister}
mkFields ''SHFInstr

data STXInstr = STXInstr {
  _opcode        :: STXOp,
  _offset        :: STXOffset}
mkFields ''STXInstr

data STKInstr = STKInstr {
  _opcode        :: STKOp,
  _register      :: Either GPRegister PCRegister}
mkFields ''STKInstr

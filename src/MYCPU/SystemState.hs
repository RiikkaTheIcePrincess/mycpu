{-# LANGUAGE TemplateHaskell, BinaryLiterals #-}
module MYCPU.SystemState where
import Control.Monad.State
import Control.Lens
--import qualified Data.Vector as V
import qualified Data.Sequence as S
import Data.Word

--TODO figure out IO
type Temp = Word8

data RegState = RegState {
                      --General-purpose registers
                      --Also known as A,B,C,D,W,X,Y,Z
                        _r0,_r1,_r2,_r3,_r4,_r5,_r6,_r7 :: Word16
                      --Processor control registers
                      , _fl :: Word16 --Flags register
                      , _pc :: Word16 --Program Counter
                      , _ps :: Word16 --Processor Status register
                      , _usp :: Word16 --User stack pointer
                      , _ssp :: Word16 --Supervisor mode stack pointer
                      --Segment registers, user mode
                      , _csu :: Word32 --Code segment
                      , _dsu :: Word32 --Data segment
                      , _esu :: Word32 --Extra data segment
                      , _ssu :: Word32 --Stack segment
                      --Segment registers, supervisor mode
                      , _css, _dss, _ess, _sss :: Word32
                      , _is :: Word32 --Interrupt segment
                      } deriving (Eq, Show)
makeLenses ''RegState

data SysState = SysState  {
                        _registers :: RegState
                      , _ioPorts :: S.Seq Temp -- ?? TODO
                      , _ramemory :: S.Seq Word8
                      , _romemory :: S.Seq Word8
                      } deriving Eq
instance Show SysState where
  show (SysState reg _ ram rom) =
    "\nRegs: " ++ show reg
    ++ "\nrom:" ++ show rom
    ++ "\nram: (big list)"-- ++ show ram
makeLenses ''SysState

--TODO set these appropriately instead of full of test values
rs0 = RegState 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
initialSystemState = SysState rs0 S.empty (S.replicate (2^16) 0) S.empty
--For testing :3
iss1 = SysState (rs0 { _r0 = 0b0111010110011011 }) S.empty (S.replicate (2^16) 0) S.empty

--TODO handle IO
rsNoMMU = rs0 { _csu = 0b11001111111100000000000000000000,
            _is  = 0xCFF00000, --Same values as CS above
            _dsu = 0x0FF00000,
            _esu = 0x0FF00000,
            _ssu = 0x0FF00000 }
initStateNoMMU = SysState rsNoMMU S.empty (S.replicate (2^16) 0) (S.replicate (2^16) 0)

--TemplateHaskell likes to make things irritating so this is down here now (:
type YCFunc = StateT SysState IO ()

data Interrupt =
  Reset
  | Clock
  | DivZeroFault
  | DoubleFault
  | StackFault
  | SegFault
  | UnprivFault
  | UndefFault
  | HWI
  | BusRefresh
  | DebugQuery
  | SWI
  deriving Show

module MYCPU where
import MYCPU.SystemState
import MYCPU.Misc
import MYCPU.BitPatterns
import MYCPU.Instructions
import Control.Monad.State
import Control.Lens

testInstr instr = execStateT (interpret instr) initialSystemState
getR0 state = state ^. registers . r0

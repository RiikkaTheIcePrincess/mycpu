{-# LANGUAGE OverloadedStrings #-}
import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Monadic

import Data.Bits
import Data.Vector hiding ((++))
import Data.Word
import Control.Lens
import Control.Monad.State

--import MYCPU
import MYCPU.Misc
import MYCPU.SystemState
import MYCPU.BitPatterns
import MYCPU.Instructions

import MYCPU.Test.ALUTests
import MYCPU.Test.BRATests
import MYCPU.Test.SHFTests
import MYCPU.Test.BTITests
import MYCPU.Test.SEITests
import MYCPU.Test.FLGTests

main :: IO ()
main = hspec $ do
  aluTests
  braTests
  shfTests
  btiTests
  seiTests
  flgTests

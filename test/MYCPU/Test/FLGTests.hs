module MYCPU.Test.FLGTests where
import Test.Hspec
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.Vector

import Data.Binary
import Data.Bits
import Data.Word
import Data.Int
import Control.Lens hiding (pre)
import Control.Monad.State

import MYCPU.Misc
import MYCPU.SystemState
import MYCPU.BitPatterns
import MYCPU.Instructions

ss0 = initialSystemState
-- r0 = 0b0111010110011011
ss1 = iss1
instance Arbitrary GPRegister where
  arbitrary = oneof $ fmap return [R0,R1,R2,R3,R4,R5,R6,R7]
testInstr i s = run $ execStateT (interpret i) s

flgTests = 
  describe "Quickcheckified FLG tests" $ parallel $ do
    it "SEF" $
      property testSEFQuick
    it "CLF" $
      property testCLFQuick

checkFlags fl n z c v = do
  --Check N
  assert $ testBit fl 0xF == n
  --Check Z
  assert $ testBit fl 0xE == z
  --Check C
  assert $ testBit fl 0xD == c
  --Check V
  assert $ testBit fl 0xC == v

testSEFQuick n z c v = monadicIO $ do
  res <- testInstr (FLGInstr SEF n z c v) ss0
  --We can test this way because ss0 has zero-initialised registers
  --So, only if we SEF a flag will it be set
  assert $ testFLN res == n
  assert $ testFLZ res == z
  assert $ testFLC res == c
  assert $ testFLV res == v

testCLFQuick n z c v = monadicIO $ do
  res <- testInstr (FLGInstr CLF n z c v) (ss0 { _registers = rs0 {_fl = 0xFFFF} } )
  --Similar reasoning, we'll 1-initialise so only
  --if a flag is cleared will it be low
  assert $ testFLN res == not n
  assert $ testFLZ res == not z
  assert $ testFLC res == not c
  assert $ testFLV res == not v

module MYCPU.Test.SEITests where
import Test.Hspec
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.Vector

import Data.Binary
import Data.Bits
import Data.Word
import Data.Int
import Control.Lens hiding (pre)
import Control.Monad.State

import MYCPU.Misc
import MYCPU.SystemState
import MYCPU.BitPatterns
import MYCPU.Instructions

ss0 = initialSystemState
-- r0 = 0b0111010110011011
ss1 = iss1
instance Arbitrary GPRegister where
  arbitrary = oneof $ fmap return [R0,R1,R2,R3,R4,R5,R6,R7]
testInstr i s = run $ execStateT (interpret i) s

seiTests = 
  describe "Quickcheckified SEI tests" $ parallel $ do
    it "SET" $
      property testSETQuick

checkFlags fl n z c v = do
  --Check N
  assert $ testBit fl 0xF == n
  --Check Z
  assert $ testBit fl 0xE == z
  --Check C
  assert $ testBit fl 0xD == c
  --Check V
  assert $ testBit fl 0xC == v

testSETQuick val dest = monadicIO $ do
  res <- testInstr (SEIInstr SET val dest) ss0
  let exp = val
  let act = res ^. registers . mapGP dest
  assert $ exp == act

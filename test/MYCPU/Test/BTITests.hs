module MYCPU.Test.BTITests where
import Test.Hspec
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.Vector

import Data.Binary
import Data.Bits
import Data.Word
import Data.Int
import Control.Lens hiding (pre)
import Control.Monad.State

import MYCPU.Misc
import MYCPU.SystemState
import MYCPU.BitPatterns
import MYCPU.Instructions

ss0 = initialSystemState
-- r0 = 0b0111010110011011
ss1 = iss1
instance Arbitrary GPRegister where
  arbitrary = oneof $ fmap return [R0,R1,R2,R3,R4,R5,R6,R7]
testInstr i s = run $ execStateT (interpret i) s

btiTests = 
  describe "Quickcheckified BTI tests" $ parallel $ do
    it "BTT" $
      property testBTTQuick
    it "BTX" $
      property testBTXQuick
    it "BTC" $
      property testBTCQuick
    it "BTS" $
      property testBTSQuick

checkFlags fl n z c v = do
  --Check N
  assert $ testBit fl 0xF == n
  --Check Z
  assert $ testBit fl 0xE == z
  --Check C
  assert $ testBit fl 0xD == c
  --Check V
  assert $ testBit fl 0xC == v

testBTTQuick val preTgt dest = monadicIO $ do
  let preTgt' = preTgt `mod` 16
  let tgt = fromIntegral $ preTgt' --Must be a bit index
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (BTIInstr BTT preTgt' dest) ss2
  let fl = _fl $ res ^. registers
  let n = testFLN ss2
  let z = not(testBit val tgt)
  let c = testFLC ss2
  let v = testFLV ss2
  checkFlags fl n z c v

testBTXQuick val preTgt dest = monadicIO $ do
  let preTgt' = preTgt `mod` 16
  let tgt = fromIntegral $ preTgt' --Must be a bit index
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (BTIInstr BTX preTgt' dest) ss2
  let exp = complementBit val tgt
  let act = res ^. registers . mapGP dest
  let fl = _fl $ res ^. registers
  let n = testFLN ss2
  let z = not(testBit val tgt)
  let c = not(z)
  let v = testFLV ss2
  checkFlags fl n z c v
  assert $ exp == act

testBTCQuick val preTgt dest = monadicIO $ do
  let preTgt' = preTgt `mod` 16
  let tgt = fromIntegral $ preTgt' --Must be a bit index
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (BTIInstr BTC preTgt' dest) ss2
  let exp = clearBit val tgt
  let act = res ^. registers . mapGP dest
  let fl = _fl $ res ^. registers
  let n = testFLN ss2
  let z = not(testBit val tgt)
  let c = val /= exp
  let v = testFLV ss2
  checkFlags fl n z c v
  assert $ exp == act

testBTSQuick val preTgt dest = monadicIO $ do
  let preTgt' = preTgt `mod` 16
  let tgt = fromIntegral $ preTgt' --Must be a bit index
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (BTIInstr BTS preTgt' dest) ss2
  let exp = setBit val tgt
  let act = res ^. registers . mapGP dest
  let fl = _fl $ res ^. registers
  let n = testFLN ss2
  let z = not(testBit val tgt)
  let c = val /= exp
  let v = testFLV ss2
  checkFlags fl n z c v
  assert $ exp == act

module MYCPU.Test.SHFTests where
import Test.Hspec
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.Vector

import Data.Binary
import Data.Bits
import Data.Word
import Data.Int
import Control.Lens hiding (pre)
import Control.Monad.State

import MYCPU.Misc
import MYCPU.SystemState
import MYCPU.BitPatterns
import MYCPU.Instructions

ss0 = initialSystemState
ss1 = iss1
instance Arbitrary GPRegister where
  arbitrary = oneof $ fmap return [R0,R1,R2,R3,R4,R5,R6,R7]
testInstr i s = run $ execStateT (interpret i) s

shfTests = do
  describe "Pre-formed SHF tests" $ do
    testROLPre
    testRORPre
  describe "Quickcheckified SHF tests" $ parallel $ do
    it "ASL" $
      property testASLQuick
    it "LSL" $
      property testLSLQuick
    it "ROL" $
      property testROLQuick
    it "RNL" $
      property testRNLQuick
    it "ASR" $
      property testASRQuick
    it "LSR" $
      property testLSRQuick
    it "ROR" $
      property testRORQuick
    it "RNR" $
      property testRNRQuick

checkFlags fl n z c v = do
  --Check N
  assert $ testBit fl 0xF == n
  --Check Z
  assert $ testBit fl 0xE == z
  --Check C
  assert $ testBit fl 0xD == c
  --Check V
  assert $ testBit fl 0xC == v

testROLPre = describe "ROL" $ do
  it "works with: ROL 3, R0" $
    execStateT (interpret $ SHFInstr ROL 3 R0) ss1 `shouldReturn`
      (ss1 { _registers = rs0 {_fl = 40960, _r0 = 44249} })
      
testRORPre = describe "ROR" $ do
  it "works with: ROR 3, R0" $
    execStateT (interpret $ SHFInstr ROR 3 R0) ss1 `shouldReturn`
      (ss1 { _registers = rs0 {_fl = 32768, _r0 = 52915} })

testASLQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (SHFInstr ASL dist dest) ss2
  let exp = shiftL (val :: Word16) (fromIntegral dist)
  let act = res ^. registers . (mapGP dest)
  let fl = _fl $ res ^. registers
  let n = testBit exp 0xF
  let z = exp == 0
  let c = popCount val > popCount exp
  let v = testFLV ss2
  checkFlags fl n z c v
  assert $ exp == act

testLSLQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (SHFInstr LSL dist dest) ss2
  let exp = shiftL (val :: Word16) (fromIntegral dist)
  let act = res ^. registers . (mapGP dest)
  let fl = _fl $ res ^. registers
  let n = testBit exp 0xF
  let z = exp == 0
  let c = popCount val > popCount exp
  let v = testFLV ss2
  checkFlags fl n z c v
  assert $ exp == act

testROLQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (SHFInstr ROL dist dest) ss2
  --Hmm, we don't test with C set at the start
  --Will likely be both high and low throughout, though
  let (exp,c) = roll False val dist
  let act = res ^. registers . (mapGP dest)
  let fl = _fl $ res ^. registers
  let n = testBit exp 0xF
  let z = exp == 0
  let v = testFLV ss2
  checkFlags fl n z c v
  assert $ exp == act

testRNLQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (SHFInstr RNL dist dest) ss2
  let exp = rotateL (val :: Word16) (fromIntegral dist)
  let act = res ^. registers . (mapGP dest)
  let fl = _fl $ res ^. registers
  let n = testBit exp 0xF
  let z = exp == 0
  let c = testFLC ss2
  let v = testFLV ss2
  checkFlags fl n z c v
  assert $ exp == act

testASRQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (SHFInstr ASR dist dest) ss2
  let exp = shiftR (fromIntegral (val :: Word16) :: Int16) (fromIntegral dist)
  let act = res ^. registers . (mapGP dest)
  let fl = _fl $ res ^. registers
  let n = testBit exp 0xF
  let z = exp == 0
  let c = popCount val > popCount exp
  let v = testFLV ss2
  checkFlags fl n z c v
  assert $ exp == fromIntegral act

testLSRQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (SHFInstr LSR dist dest) ss2
  let exp = shiftR (val :: Word16) (fromIntegral dist)
  let act = res ^. registers . (mapGP dest)
  let fl = _fl $ res ^. registers
  let n = testBit exp 0xF
  let z = exp == 0
  let c = popCount val > popCount exp
  let v = testFLV ss2
  checkFlags fl n z c v
  assert $ exp == act

testRORQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (SHFInstr ROR dist dest) ss2
  --Hmm, we don't test with C set at the start
  --Will likely be both high and low throughout, though
  let (exp,c) = rock False val dist
  let act = res ^. registers . (mapGP dest)
  let fl = _fl $ res ^. registers
  let n = testBit exp 0xF
  let z = exp == 0
  let v = testFLV ss2
  checkFlags fl n z c v
  assert $ exp == act

testRNRQuick val preDist dest = monadicIO $ do
  let dist = preDist `mod` 17 --Spec says dist is on [0,16]
  let ss2 = ss0 & registers . (mapGP dest) .~ val
  res <- testInstr (SHFInstr RNR dist dest) ss2
  let exp = rotateR (val :: Word16) (fromIntegral dist)
  let act = res ^. registers . (mapGP dest)
  let fl = _fl $ res ^. registers
  let n = testBit exp 0xF
  let z = exp == 0
  let c = testFLC ss2
  let v = testFLV ss2
  checkFlags fl n z c v
  assert $ exp == act

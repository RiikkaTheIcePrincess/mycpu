{-# LANGUAGE OverloadedStrings, BinaryLiterals, FlexibleInstances #-}
module MYCPU.Test.ALUTests where
import Test.Hspec
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.Vector

import Prelude hiding (length)
import Data.Binary
import Data.Binary.Get
import Data.Bits
import Data.Int
import qualified Data.ByteString.Lazy as BSL (take, drop)
--import Data.Vector hiding ((++), generate)
import Data.Sequence hiding ((++), generate)
import Data.Word
import Data.Maybe (fromMaybe)
import Control.Lens hiding (pre)
import Control.Monad.State
import qualified Control.Monad as CM

--import MYCPU
import MYCPU.Misc
import MYCPU.SystemState
import MYCPU.BitPatterns
import MYCPU.Instructions

ss0 = initialSystemState
rs1 = rs0{_r1=1,_r2=2,_r3=3,_r4=4,_r5=5,_r6=6,_r7=7} 
ss1 = ss0 { _ramemory=fromList [0,1,2,3,4,5,6,7,8,9,10],
            _registers=rs1}
ss1_16 = ss0 { _ramemory=fromList [0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,0,8,0,9,0,10,0],
            _registers=rs1}
aluinstr op v ebm reg = ALUInstr op (Addr (Immediate v)) ebm False reg
instance Arbitrary GPRegister where
  arbitrary = oneof $ fmap return [R0,R1,R2,R3,R4,R5,R6,R7]
instance Arbitrary IxRegister where
  arbitrary = oneof $ fmap return [W,X,Y,Z]
instance Arbitrary ALUAddressing where
  arbitrary = oneof [Addr <$> arbitrary, ControlRegister <$> arbitrary]
instance Arbitrary PCRegister where
  arbitrary = oneof $ fmap return [FL,PC,PS,USP,SSP]
instance Arbitrary JAddressing where
  arbitrary = oneof
    [Immediate <$> arbitrary
    ,Absolute <$> arbitrary
    ,Register <$> arbitrary
    ,Indirect <$> arbitrary
    ,liftM2 IndirectOffset arbitrary arbitrary
    ,liftM2 IndirectIndexed arbitrary arbitrary]
testInstr i s = run $ execStateT (interpret i) s

aluTests = do
  describe "Pre-formed ALU tests" $ parallel $ do
    compositePre
    testCMPPre
    testNEGPre
    testADDPre
    testSUBPre
    testADCPre
    testMLIPre
  describe "QuickCheckified ALU tests" $ parallel $ do
    testCMPQuick
    testNEGQuick
    testADDQuick
    testSUBQuick
    testADCQuick
    testSBCQuick
    testMULQuick
    testDIVQuick
    testMLIQuick
    testDVIQuick
    testMODQuick
    testMDIQuick
    testANDQuick
    testORRQuick
    testEORQuick
    testNOTQuick
    testLODQuick
    testSTOQuick


--Temporary, should probably do this at a lower level
--before we read out operands, so we can just say to add registers
--rather than having to pluck out values from the state
compositePre = describe "Composite tests" $ do
  let inst0 = ALUInstr ADD (Addr (Immediate 7)) True False R0
  state0 <- runIO $ execStateT (interpret inst0) ss1
  let inst1 = ALUInstr ADD (Addr (Absolute 7)) True False R0
  state1 <- runIO $ execStateT (interpret inst1) state0
  let inst2 = ALUInstr MUL (Addr (Register R2)) True False R0
  state2 <- runIO $ execStateT (interpret inst2) state1
  let inst3 = ALUInstr DIV (Addr (Indirect R4)) True False R0
  state3 <- runIO $ execStateT (interpret inst3) state2
  let inst4 = ALUInstr SUB (Addr (Immediate 1)) True False R0
  state4 <- runIO $ execStateT (interpret inst4) state3
  it "Calculates ((((7+7)*2)/4)-1) == 6" $
    state4 `shouldSatisfy` (\s -> _fl (s ^. registers) == 0 && _r0 (s ^. registers) == 6)

testCMPPre = describe "CMP" $ parallel $ --do
  it "works with: CMP, 0 ? 0, 16-bit, R0" $
    execStateT (interpret $ ALUInstr CMP (Addr (Immediate 0)) False False R0) ss1 `shouldReturn`
      (ss1 { _registers = rs1 {_fl = 0xE000} })

testNEGPre = describe "NEG" $ do
  it "works with: NEG, 0, 8-bit, R0" $
    execStateT (interpret $ aluinstr NEG 0 True R0) ss1 `shouldReturn`
      (ss1 { _registers = rs1 {_fl = 0b0100000000000000} })
  it "works with: NEG, 1, 8-bit, R0" $
    execStateT (interpret $ aluinstr NEG 1 True R0) ss1 `shouldReturn`
      (ss1 { _registers = rs1 {_fl = 0b1000000000000000, _r0 = 0xFF} })
  it "works with: NEG, 0, 16-bit, R0" $
    execStateT (interpret $ aluinstr NEG 0 False R0) ss1 `shouldReturn`
      (ss1 { _registers = rs1 {_fl = 0b0100000000000000} })
  it "works with: NEG, 1, 16-bit, R0" $
    execStateT (interpret $ aluinstr NEG 1 False R0) ss1 `shouldReturn`
      (ss1 { _registers = rs1 {_fl = 0b1000000000000000, _r0 = 0xFFFF} })
  it "works with: NEG, 1, 16-bit, R1" $
    execStateT (interpret $ aluinstr NEG 1 False R1) ss1 `shouldReturn`
      (ss1 { _registers = rs1 {_fl = 0b1000000000000000, _r1 = 0xFFFF} })

testADDPre = describe "ADD" $ do
  let ss2 = ss0 { _registers=rs0 {_r3 = 89} }
  it "works with: ADD, 39, 89, 8-bit, R3" $
    execStateT (interpret $ aluinstr ADD 39 True R3) ss2 `shouldReturn`
      (ss2 { _registers = rs0 {_r3 = 128, _fl = 0b1001000000000000} })
  let ss3 = ss0 { _registers=rs0 {_r6 = 75} }
  it "works with: ADD, 53, 75, 8-bit, R6" $
    execStateT (interpret $ aluinstr ADD 53 True R6) ss3 `shouldReturn`
      (ss3 { _registers = rs0 {_r6 = 128, _fl = 0b1001000000000000} })

testSUBPre = describe "SUB" $ do
  it "works with: SUB, 0, 1, 8-bit, R0" $
    execStateT (interpret $ aluinstr SUB 1 True R0) ss1 `shouldReturn`
      (ss1 { _registers = rs1 {_r0 = 255, _fl = 0b1010000000000000} })
  it "works with: SUB, 0, 1, 16-bit, R0" $
    execStateT (interpret $ aluinstr SUB 1 False R0) ss1 `shouldReturn`
      (ss1 { _registers = rs1 {_r0 = 65535, _fl = 0b1010000000000000} })
  it "works with: SUB, 6, 0, 16-bit, R1" $
    execStateT (interpret $ aluinstr SUB 0 False R6) ss1 `shouldReturn`
      (ss1 { _registers = rs1 {_r6 = 6, _fl = 0b0000000000000000} })

testADCPre = describe "ADC" $ do
  let ss2 = ss0 { _registers = rs1 { _r0=40, _fl = 0b0010000000000000 } }
  it "works with: ADC, 40, 87, 8-bit, R0" $
    execStateT (interpret $ aluinstr ADC 87 True R0) ss2 `shouldReturn`
      (ss2 { _registers = rs1 {_r0 = 128, _fl = 0b1001000000000000} })

testMLIPre = describe "MLI" $ do
  it "works with: MLI, -1, 7, 8-bit, R7" $
    execStateT (interpret $ aluinstr MLI (255) True R7) ss1 `shouldReturn`
      (ss1 { _registers = rs1 {_r0 = 0xFF, _r7 = fromIntegral (249 :: Word8), _fl = 0b1010000000000000} })
  it "works with: MLI, -1, 7, 16-bit, R7" $
    execStateT (interpret $ aluinstr MLI (65535) False R7) ss1 `shouldReturn`
      (ss1 { _registers = rs1 {_r0 = 0xFFFF, _r7 = (65529), _fl = 0b1010000000000000} }) 

testCMPQuick = describe "CMP" $ do
  it "compares 8-bit stuff correctly" $
    property cmpQuick8
  it "compares 16-bit stuff correctly" $
    property cmpQuick

testNEGQuick = describe "NEG" $ do
  it "negates 8-bit stuff correctly" $
    property negQuick8
  it "negates 16-bit stuff correctly" $
    property negQuick

testADDQuick = describe "ADD" $ do
  it "adds 8-bit stuff correctly" $
    property addQuick8
  it "adds 16-bit stuff correctly" $
    property addQuick

testSUBQuick = describe "SUB" $ do
  it "subtracts 8-bit stuff correctly" $
    property subQuick8
  it "subtracts 16-bit stuff correctly" $
    property subQuick

testADCQuick = describe "ADC" $ do
  it "adds 8-bit thingums with carry correctly" $
    property adcQuick8
  it "adds 16-bit critters with carry correctly" $
    property adcQuick

testSBCQuick = describe "SBC" $ do
  it "subtracts 8-bit whatsits with carry correctly" $
    property sbcQuick8
  it "subtracts 16-bit gubbins with carry correctly" $
    property sbcQuick

testMULQuick = describe "MUL" $ do
  it "does the 8-bit multiply thing" $
    property mulQuick8
  it "does the 16-bit multiply thing" $
    property mulQuick

testDIVQuick = describe "DIV" $ do
  it "dances the 8-bit division shuffle" $
    property divQuick8
  it "dances the 16-bit division shuffle" $
    property divQuick

testMLIQuick = describe "MLI" $ do
  it "signedly multiplies 8-bit-ish stuff" $
    property mliQuick8
  it "signedly multiplies 16-bitly" $
    property mliQuick

testDVIQuick = describe "DVI" $ do
  it "signedly divides 8-bitters" $
    property dviQuick8
  it "signedly divides 16-bitlets" $
    property dviQuick

testMODQuick = describe "MOD" $ do
  it "moduluses 8-bit whatevers" $
    property modQuick8
  it "moduluses 16-bit things" $
    property modQuick

testMDIQuick = describe "MDI" $ do
  it "signedly moduluses 8-bit things" $
    property mdiQuick8
  it "signedly moduluses 16-bit whatevers" $
    property mdiQuick

testANDQuick = describe "AND" $ do
  it "ANDs 8-bit things" $
    property andQuick8
  it "ANDs 16-bit whatevers" $
    property andQuick

testORRQuick = describe "ORR" $ do
  it "ORRs 8-bit things" $
    property orrQuick8
  it "ORRs 16-bit whatevers" $
    property orrQuick

testEORQuick = describe "EOR" $ do
  it "X/EORs 8-bit things" $
    property eorQuick8
  it "X/EORs 16-bit whatevers" $
    property eorQuick

testNOTQuick = describe "NOT" $ do
  it "NOTs 8-bit things" $
    property notQuick8
  it "NOTs 16-bit whatevers" $
    property notQuick --[Joke] Very slow D:

testLODQuick = describe "LOD" $ do
  it "LODs 8-bit things" $
    property lodQuick8
  it "LODs 16-bit whatevers" $
    property lodQuick

testSTOQuick = describe "STO" $ do
  it "STOs 8-bit things" $
    property stoQuick8
  it "STOs 16-bit whatevers" $
    property stoQuick

cheatyReadOp s eds op2 =
  case readOperand s eds op2 of
    Left irpt -> error "FIXME handle interrupts"
    Right v -> v

cmpQuick8 op1 op2 eds reg = monadicIO $ do
  let sstest = ss0 { _registers = (rs0 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr CMP op2 True eds reg) sstest
  --Why the crap does this not do what I expect?
  --Works in GHCi, 'registers . fl' works in MYCPU.Instructions...
  --let fl = res ^. registers . fl
  let fl = _fl $ res ^. registers
  let op1w = res ^. registers . (mapGP reg)
  let op2w = cheatyReadOp sstest eds op2
  let op1' = op1w .&. 0xFF
  let op2' = op2w .&. 0xFF
  --Test N(egative) flag (op1>=op2 signed)
  assert $ testBit fl 0xF == (w16ToInt8 op1w >= w16ToInt8 op2w)
  --Test Z(ero) flag (op1==op2)
  assert $ testBit fl 0xE == (op1' == op2')
  --Test C(arry) flag (op1>=op2 unsigned)
  assert $ testBit fl 0xD == (op1' >= op2')

cmpQuick op1 op2 eds reg = monadicIO $ do
  let sstest = ss0 { _registers = (rs0 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr CMP op2 False eds reg) sstest
  let fl = _fl $ res ^. registers
  let op1w = res ^. registers . (mapGP reg)
  let op2w = cheatyReadOp sstest eds op2
  --Test N(egative) flag (op1>=op2 signed)
  assert $ testBit fl 0xF == (w16ToInt16 op1w >= w16ToInt16 op2w)
  --Test Z(ero) flag (op1==op2)
  assert $ testBit fl 0xE == (op1w == op2w)
  --Test C(arry) flag (op1>=op2 unsigned)
  assert $ testBit fl 0xD == (op1w >= op2w)
  --TODO Consider testing 'not affected' flags just in case?
  --It /may/ be possible for me to do something that silly...

negQuick8 op2 eds reg = monadicIO $ do
  res <- testInstr (ALUInstr NEG op2 True eds reg) ss0
  let fl = _fl $ res ^. registers
  let op2w = cheatyReadOp ss0 eds op2
  let newVal = negate (fromIntegral op2w :: Int8) :: Int8
  --Check N(egative) flag (set if bit 7 is set)
  assert $ testBit fl 0xF == testBit newVal 0x7
  --Check Z(ero) flag (set if zero)
  assert $ testBit fl 0xE == (newVal == 0)
  --Check o(V)erflow flag (set if we tried to negate 0x80)
  assert $ testBit fl 0xC == (newVal == (-0x80))
  --Check the register too
  --Maybe find another way to test this than relying on
  --  the same conversion code?
  assert $ res ^. registers . mapGP reg == int8ToW16 newVal
  
negQuick op2 eds reg = monadicIO $ do
  res <- testInstr (ALUInstr NEG op2 False eds reg) ss0
  let fl = _fl $ res ^. registers
  let op2w = cheatyReadOp ss0 eds op2
  let newVal = negate(fromIntegral op2w :: Int16) :: Int16
  --Check N(egative) flag (set if bit 15 is set)
  assert $ testBit fl 0xF == testBit newVal 0xF
  --Check Z(ero) flag (set if zero)
  assert $ testBit fl 0xE == (newVal == 0)
  --Check o(V)erflow flag (set if we tried to negate 0x80)
  assert $ testBit fl 0xC == (newVal == (-0x8000))
  --Check the register too
  --Maybe find another way to test this than relying on
  --  the same conversion code?
  let regV = res ^. registers . mapGP reg
  assert $ regV == int16ToW16 newVal
  assert $ w16ToInt16 regV == newVal

--checkFlags :: Word16 -> Bool -> Bool -> Bool -> Bool -> a
checkFlags fl n z c v = do
  --Check N
  assert $ testBit fl 0xF == n
  --Check Z
  assert $ testBit fl 0xE == z
  --Check C
  assert $ testBit fl 0xD == c
  --Check V
  assert $ testBit fl 0xC == v

calcSums op1U op2U op1S op2S =
  ( op1U + op2U, --sumU
    fromIntegral op1S + fromIntegral op2S :: Int32, --bigSumS
    fromIntegral op1U + fromIntegral op2U :: Word32) --bigSumU
calcSubs op1U op2U op1S op2S =
  ( op1U - op2U, --subU
    fromIntegral op1S - fromIntegral op2S :: Int32, --bigSubS
    fromIntegral op1U - fromIntegral op2U :: Word32) --bigSubU

checkAddSubFlags8 add op1 op2 res = do
  let fl = _fl $ res ^. registers
  let op1U = w16ToW8 op1
  let op1S = w8ToInt8  op1U
  let op2U = w16ToW8 op2
  let op2S = w8ToInt8  op2U
  let (sumU,bigSumS,bigSumU) = 
        if add then calcSums op1U op2U op1S op2S
          else calcSubs op1U op2U op1S op2S
  let n = testBit sumU 0x7
  let z = sumU == 0
  let c = bigSumU > 0xFF
  let v = bigSumS < (-0x80) || bigSumS >= 0x80
  checkFlags fl n z c v
  return $ w8ToW16 sumU

checkAddSubFlags add op1 op2 res = do
  let fl = _fl $ res ^. registers
  let op1U = op1
  let op1S = w16ToInt16  op1U
  let op2U = op2
  let op2S = w16ToInt16  op2U
  let (sumU,bigSumS,bigSumU) =
        if add then calcSums op1U op2U op1S op2S
          else calcSubs op1U op2U op1S op2S
  let n = testBit sumU 0xF
  let z = sumU == 0
  let c = bigSumU > 0xFFFF
  let v = bigSumS < (-0x8000) || bigSumS >= 0x8000
  checkFlags fl n z c v
  return sumU
  
addQuick8 op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs0 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr ADD op2 True eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  --Seems there aren't many ways to go about this.
  --Ugh. Just... test everything's set? I don't know what else to do here.
  sumU <- checkAddSubFlags8 True op1 op2' res
  --Check register
  assert $ res ^. registers . mapGP reg == sumU
  
addQuick op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs0 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr ADD op2 False eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  sumU <- checkAddSubFlags True op1 op2' res
  --Check register
  assert $ res ^. registers . mapGP reg == sumU

subQuick8 op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs0 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr SUB op2 True eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  sumU <- checkAddSubFlags8 False op1 op2' res
  --Check register
  assert $ res ^. registers . mapGP reg == sumU

subQuick op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs0 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr SUB op2 False eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  sumU <- checkAddSubFlags False op1 op2' res
  --Check register
  assert $ res ^. registers . mapGP reg == sumU

adcQuick8 op1 op2 eds reg = monadicIO $ do
  --Set carry flag high for testing
  let regs = rs1 { _fl = 0b0010000000000000 }
  let sstest = ss1 { _registers = (regs & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr ADC op2 True eds reg) sstest
  let fl = _fl $ res ^. registers
  let op2' = cheatyReadOp sstest eds op2
  sumU <- checkAddSubFlags8 True op1 (op2'+1) res
  assert $ res ^. registers . mapGP reg == sumU

adcQuick op1 op2 eds reg = monadicIO $ do
  --Set carry flag high for testing
  let regs = rs1 { _fl = 0b0010000000000000 }
  let sstest = ss1 { _registers = (regs & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr ADC op2 False eds reg) sstest
  let fl = _fl $ res ^. registers
  let op2' = cheatyReadOp sstest eds op2
  sumU <- checkAddSubFlags True op1 (op2'+1) res
  assert $ res ^. registers . mapGP reg == sumU

sbcQuick8 op1 op2 eds reg cFlag = monadicIO $ do
  let regs = if cFlag
        then rs1 { _fl = 0b0010000000000000 }
        else rs1
  let sstest = ss1 { _registers = (regs & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr SBC op2 True eds reg) sstest
  --Testing the pre-execution state to see what result should be
  let fl = _fl regs
  let op2' = cheatyReadOp sstest eds op2
  let c = testBit fl 0xD
  let c' = if c then 0 else 1
  sumU <- checkAddSubFlags8 False op1 (op2'+c') res
  assert $ res ^. registers . mapGP reg == sumU

sbcQuick op1 op2 eds reg cFlag = monadicIO $ do
  let regs = if cFlag
        then rs1 { _fl = 0b0010000000000000 }
        else rs1
  let sstest = ss1 { _registers = (regs & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr SBC op2 False eds reg) sstest
  --Testing the pre-execution state to see what result should be
  let fl = _fl regs
  let op2' = cheatyReadOp sstest eds op2
  let c = testBit fl 0xD
  let c' = if c then 0 else 1
  sumU <- checkAddSubFlags False op1 (op2'+c') res
  assert $ res ^. registers . mapGP reg == sumU

mulQuick8 op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr MUL op2 True eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let prod = (op1 .&. 0x00FF) * (op2' .&. 0x00FF)
  let fl = _fl $ res ^. registers
  let n = False
  let z = prod == 0
  let c = prod .&. 0xFF00 /= 0
  let v = testBit (_fl $ sstest ^. registers) 0xC
  checkFlags fl n z c v
  assert $ res ^. registers . mapGP R0 ==
    --R0 is overwritten if it's dest
    if reg == R0 then prod .&. 0x00FF
      else prod .&. 0xFF00
  assert $ res ^. registers . mapGP reg == prod .&. 0x00FF

mulQuick op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr MUL op2 False eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let prod = (w16ToW32 op1) * (w16ToW32 op2') :: Word32
  let fl = _fl $ res ^. registers
  let n = False
  let z = prod == 0
  let c = prod .&. 0xFFFF0000 /= 0
  let v = testBit (_fl $ sstest ^. registers) 0xC
  let hw     = prod .&. 0xFFFF0000
  let hiWord = runGet getWord16be $ BSL.take 2 $ encode hw
  let loWord = fromIntegral $ prod .&. 0x0000FFFF :: Word16
  checkFlags fl n z c v
  assert $ res ^. registers . mapGP R0 ==
    if reg == R0 then loWord
      else hiWord
  assert $ res ^. registers . mapGP reg == loWord

--TODO check DivZeroFault when implemented
divQuick8 op1 op2 reg
  | op2 .&. 0x00FF == 0 = discard --Discard this test case
  | otherwise = monadicIO $ do
    let sstest = ss0 { _registers = (rs0 & mapGP reg .~ op1) }
    res <- testInstr (aluinstr DIV op2 True reg) sstest
    let quot = (op1 .&. 0x00FF) `div` (op2 .&. 0x00FF)
    let fl = _fl $ res ^. registers
    let n = False
    let z = quot == 0
    let c = testBit (_fl $ sstest ^. registers) 0xD
    let v = testBit (_fl $ sstest ^. registers) 0xC
    checkFlags fl n z c v
    assert $ res ^. registers . mapGP reg == quot

--TODO check DivZeroFault here too
divQuick op1 op2 reg
  | op2 == 0 = discard
  | otherwise = monadicIO $ do
    let sstest = ss0 { _registers = (rs0 & mapGP reg .~ op1) }
    res <- testInstr (aluinstr DIV op2 False reg) sstest
    let quot = op1 `div` op2
    let fl = _fl $ res ^. registers
    let n = False
    let z = quot == 0
    let c = testBit (_fl $ ss0 ^. registers) 0xD
    let v = testBit (_fl $ ss0 ^. registers) 0xC
    checkFlags fl n z c v
    assert $ res ^. registers . mapGP reg == quot

mliQuick8 op1 op2 eds reg = monadicIO $ do
  --Really sick of just testing the data is set
  --rather than testing the maths/logics
  let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr MLI op2 True eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let lil1 = fromIntegral (w8ToInt8 $ w16ToW8 op1) :: Int16
  let lil2 = fromIntegral (w8ToInt8 $ w16ToW8 op2') :: Int16
  let prod = lil1 * lil2
  let loByte = runGet getInt8 $ BSL.drop 1 $ encode prod
  let hiByte = runGet getInt8 $ BSL.take 1 $ encode prod
  let fl = _fl $ res ^. registers
  let n = testBit hiByte 0x7
  let z = prod == 0
  let c = hiByte /= 0
  let v = testBit (_fl $ ss0 ^. registers) 0xC
  checkFlags fl n z c v
  let prodU = runGet getWord16be $ encode prod
  assert $ res ^. registers . mapGP R0 ==
    --R0 is overwritten if it's dest
    if reg == R0 then prodU .&. 0x00FF
      else shiftR prodU 8
  assert $ res ^. registers . mapGP reg == prodU .&. 0x00FF

mliQuick op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr MLI op2 False eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let lil1 = w16ToInt32 op1 :: Int32
  let lil2 = w16ToInt32 op2' :: Int32
  let prod = lil1 * lil2
  let loWord = runGet getInt16be $ BSL.drop 2 $ encode prod
  let hiWord = runGet getInt16be $ BSL.take 2 $ encode prod
  let fl = _fl $ res ^. registers
  let n = testBit hiWord 0xF
  let z = prod == 0
  let c = hiWord /= 0
  let v = testBit (_fl $ sstest ^. registers) 0xC
  checkFlags fl n z c v
  let prodU = runGet getWord32be $ encode prod
  assert $ res ^. registers . mapGP R0 ==
    --R0 is overwritten if it's dest
    if reg == R0 then fromIntegral (prodU .&. 0x0000FFFF)
      else fromIntegral (shiftR prodU 16)
  assert $ res ^. registers . mapGP reg == fromIntegral (prodU .&. 0x0000FFFF)

--TODO check DivZeroFault when implemented
--Leaving out ALUAddressing randomisation (op2) for simplicity
dviQuick8 op1 op2 reg
  | op2 .&. 0x00FF == 0 = discard
  | otherwise = monadicIO $ do
    let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
    res <- testInstr (aluinstr DVI op2 True reg) sstest
    let lil1 = w8ToInt8 $ w16ToW8 op1 :: Int8
    let lil2 = w8ToInt8 $ w16ToW8 op2 :: Int8
    let result = lil1 `div` lil2
    let fl = _fl $ res ^. registers
    let n = False
    let z = result == 0
    let c = testBit (_fl $ sstest ^. registers) 0xD
    let v = testBit (_fl $ sstest ^. registers) 0xC
    let result' = int8ToW16 result
    checkFlags fl n z c v
    assert $ res ^. registers . mapGP reg == result' .&. 0x00FF

--TODO check DivZeroFault when implemented
dviQuick op1 op2 reg
  | op2 == 0 = discard
  | otherwise = monadicIO $ do
    let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
    res <- testInstr (aluinstr DVI op2 False reg) sstest
    let lil1 = w16ToInt16 op1 :: Int16
    let lil2 = w16ToInt16 op2 :: Int16
    let result = lil1 `div` lil2
    let fl = _fl $ res ^. registers
    let n = False
    let z = result == 0
    let c = testBit (_fl $ sstest ^. registers) 0xD
    let v = testBit (_fl $ sstest ^. registers) 0xC
    let result' = int16ToW16 result
    checkFlags fl n z c v
    assert $ res ^. registers . mapGP reg == result'

--TODO check DivZeroFault when implemented
modQuick8 op1 op2 reg
  | op2 .&. 0x00FF == 0 = discard --Discard this test case
  | otherwise = monadicIO $ do
    let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
    res <- testInstr (aluinstr MOD op2 True reg) sstest
    let lilMod = (op1 .&. 0x00FF) `mod` (op2 .&. 0x00FF)
    let fl = _fl $ res ^. registers
    let n = testBit lilMod 0x7
    let z = lilMod == 0
    let c = testBit (_fl $ sstest ^. registers) 0xD
    let v = testBit (_fl $ sstest ^. registers) 0xC
    checkFlags fl n z c v
    assert $ res ^. registers . mapGP reg == lilMod

--TODO check DivZeroFault here too
modQuick op1 op2 reg
  | op2 == 0 = discard
  | otherwise = monadicIO $ do
    let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
    res <- testInstr (aluinstr MOD op2 False reg) sstest
    let bigMod = op1 `mod` op2
    let fl = _fl $ res ^. registers
    let n = testBit bigMod 0xF
    let z = bigMod == 0
    let c = testBit (_fl $ ss0 ^. registers) 0xD
    let v = testBit (_fl $ ss0 ^. registers) 0xC
    checkFlags fl n z c v
    assert $ res ^. registers . mapGP reg == bigMod

--TODO check DivZeroFault when implemented
mdiQuick8 op1 op2 reg
  | op2 .&. 0x00FF == 0 = discard
  | otherwise = monadicIO $ do
    let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
    res <- testInstr (aluinstr MDI op2 True reg) sstest
    let lil1 = w8ToInt8 $ w16ToW8 op1 :: Int8
    let lil2 = w8ToInt8 $ w16ToW8 op2 :: Int8
    let result = lil1 `mod` lil2
    let fl = _fl $ res ^. registers
    let n = testBit result 0x7
    let z = result == 0
    let c = testBit (_fl $ ss0 ^. registers) 0xD
    let v = testBit (_fl $ ss0 ^. registers) 0xC
    let result' = int8ToW16 result
    checkFlags fl n z c v
    assert $ res ^. registers . mapGP reg == result' .&. 0x00FF

--TODO check DivZeroFault when implemented
mdiQuick op1 op2 reg
  | op2 == 0 = discard
  | otherwise = monadicIO $ do
    let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
    res <- testInstr (aluinstr MDI op2 False reg) sstest
    let lil1 = w16ToInt16 op1 :: Int16
    let lil2 = w16ToInt16 op2 :: Int16
    let result = lil1 `mod` lil2
    let fl = _fl $ res ^. registers
    let n = testBit result 0xF
    let z = result == 0
    let c = testBit (_fl $ sstest ^. registers) 0xD
    let v = testBit (_fl $ sstest ^. registers) 0xC
    let result' = int16ToW16 result
    checkFlags fl n z c v
    assert $ res ^. registers . mapGP reg == result'

andQuick8 op1 op2 eds reg = monadicIO $ do
    let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
    res <- testInstr (ALUInstr AND op2 True eds reg) sstest
    let op2' = cheatyReadOp sstest eds op2
    let lilResult = (op1 .&. 0x00FF) .&. (op2' .&. 0x00FF)
    let fl = _fl $ res ^. registers
    let n = testBit lilResult 0x7
    let z = lilResult == 0
    let c = testBit (_fl $ sstest ^. registers) 0xD
    let v = testBit (_fl $ sstest ^. registers) 0xC
    checkFlags fl n z c v
    assert $ res ^. registers . mapGP reg == lilResult

andQuick op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr AND op2 False eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let result = op1 .&. op2'
  let fl = _fl $ res ^. registers
  let n = testBit result 0xF
  let z = result == 0
  let c = testBit (_fl $ sstest ^. registers) 0xD
  let v = testBit (_fl $ sstest ^. registers) 0xC
  checkFlags fl n z c v
  assert $ res ^. registers . mapGP reg == result

orrQuick8 op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr ORR op2 True eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let lilResult = (op1 .&. 0x00FF) .|. (op2' .&. 0x00FF)
  let fl = _fl $ res ^. registers
  let n = testBit lilResult 0x7
  let z = lilResult == 0
  let c = testBit (_fl $ sstest ^. registers) 0xD
  let v = testBit (_fl $ sstest ^. registers) 0xC
  checkFlags fl n z c v
  assert $ res ^. registers . mapGP reg == lilResult

orrQuick op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr ORR op2 False eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let result = op1 .|. op2'
  let fl = _fl $ res ^. registers
  let n = testBit result 0xF
  let z = result == 0
  let c = testBit (_fl $ sstest ^. registers) 0xD
  let v = testBit (_fl $ sstest ^. registers) 0xC
  checkFlags fl n z c v
  assert $ res ^. registers . mapGP reg == result

eorQuick8 op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr EOR op2 True eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let lilResult = (op1 .&. 0x00FF) `xor` (op2' .&. 0x00FF)
  let fl = _fl $ res ^. registers
  let n = testBit lilResult 0x7
  let z = lilResult == 0
  let c = testBit (_fl $ sstest ^. registers) 0xD
  let v = testBit (_fl $ sstest ^. registers) 0xC
  checkFlags fl n z c v
  assert $ res ^. registers . mapGP reg == lilResult

eorQuick op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr EOR op2 False eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let result = op1 `xor` op2'
  let fl = _fl $ res ^. registers
  let n = testBit result 0xF
  let z = result == 0
  let c = testBit (_fl $ sstest ^. registers) 0xD
  let v = testBit (_fl $ sstest ^. registers) 0xC
  checkFlags fl n z c v
  assert $ res ^. registers . mapGP reg == result

--NOT operates on op2! D:
notQuick8 op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr NOT op2 True eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let lilResult = complement (op2' .&. 0x00FF) .&. 0x00FF
  let fl = _fl $ res ^. registers
  let n = testBit lilResult 0x7
  let z = lilResult == 0
  let c = testBit (_fl $ sstest ^. registers) 0xD
  let v = testBit (_fl $ sstest ^. registers) 0xC
  checkFlags fl n z c v
  assert $ res ^. registers . mapGP reg == lilResult

notQuick op1 op2 eds reg = monadicIO $ do
  let sstest = ss1 { _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr NOT op2 False eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let result = complement op2'
  let fl = _fl $ res ^. registers
  let n = testBit result 0xF
  let z = result == 0
  let c = testBit (_fl $ sstest ^. registers) 0xD
  let v = testBit (_fl $ sstest ^. registers) 0xC
  checkFlags fl n z c v
  assert $ res ^. registers . mapGP reg == result

--addr to mem, reg is register destination
lodQuick8 op2 reg eds ram = monadicIO $ do
  let sstest = ss1 {_ramemory = ram}
  res <- testInstr (ALUInstr LOD op2 True eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let addr' = fromIntegral $ op2'
  let result = _getMem ram addr' True --readMem' ram addr'
  let fl = _fl $ res ^. registers
  let n = testBit result 0x7
  let z = result == 0
  let c = testBit (_fl $ ss1 ^. registers) 0xD
  let v = testBit (_fl $ ss1 ^. registers) 0xC
  checkFlags fl n z c v
  assert $ res ^. registers . mapGP reg == result

lodQuick op2 reg eds ram = monadicIO $ do
  let sstest = ss1 {_ramemory = ram}
  res <- testInstr (ALUInstr LOD op2 False eds reg) sstest
  let op2' = cheatyReadOp sstest eds op2
  let addr' = fromIntegral op2'
  let result = _getMem ram addr' False
  let fl = _fl $ res ^. registers
  let n = testBit result 0xF
  let z = result == 0
  let c = testBit (_fl $ ss1 ^. registers) 0xD
  let v = testBit (_fl $ ss1 ^. registers) 0xC
  checkFlags fl n z c v
  assert $ res ^. registers . mapGP reg == result

--TODO test register STO too I guess
--Also maybe come up with a fancy way to test all addressing modes
stoQuick8 :: Word16 -> Word16 -> Bool -> GPRegister -> Seq Word8 -> Property
stoQuick8 op1 op2 eds reg ram = monadicIO $ do
  let sstest = ss1 { _ramemory = ram, _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr STO (Addr (Absolute op2)) True False reg) sstest
  --TODO test supervisor only STOs/interrupts
  --let op2' = cheatyReadOp ss0 eds op2
  let val = op1 .&. 0x00FF
  let op2' = fromIntegral op2
  let fl = _fl $ res ^. registers
  let n = testBit (_fl $ sstest ^. registers) 0xF
  let z = testBit (_fl $ sstest ^. registers) 0xE
  let c = testBit (_fl $ sstest ^. registers) 0xD
  let v = testBit (_fl $ sstest ^. registers) 0xC
  checkFlags fl n z c v
  if op2' < length (_ramemory res) then
    assert $ _getMem (res ^. ramemory) op2' True == val
    else
      assert $ _getMem (res ^. ramemory) op2' True == 0

stoQuick :: Word16 -> Word16 -> Bool -> GPRegister -> Seq Word8 -> Property
stoQuick op1 op2 eds reg ram = monadicIO $ do
  let sstest = ss1_16 { _ramemory = ram, _registers = (rs1 & mapGP reg .~ op1) }
  res <- testInstr (ALUInstr STO (Addr (Absolute op2)) False False reg) sstest
  let op2' = fromIntegral op2
  --No reason flags would change... should this be here?
  let fl = _fl $ res ^. registers
  let n = testBit (_fl $ sstest ^. registers) 0xF
  let z = testBit (_fl $ sstest ^. registers) 0xE
  let c = testBit (_fl $ sstest ^. registers) 0xD
  let v = testBit (_fl $ sstest ^. registers) 0xC
  checkFlags fl n z c v
  --We're looking for two bytes here!
  --Oh, I don't really want to deal with a one-byte return :(
  pre (length ram > (op2'+1) || length ram <= op2')
  if op2' + 1 < length ram then
    assert $ _getMem (res ^. ramemory) op2' False == op1
    else
      assert $ _getMem (res ^. ramemory) op2' False == 0

module MYCPU.Test.BRATests where
import Test.Hspec
import Test.QuickCheck hiding ((.&.))
import Test.QuickCheck.Monadic
import Test.QuickCheck.Instances.Vector

import Data.Binary
import Data.Bits
import Data.Word
import Data.Int
import Data.Sequence
import Control.Lens
import Control.Monad.State

import MYCPU.Misc
import MYCPU.SystemState
import MYCPU.BitPatterns
import MYCPU.Instructions

ss0 = initialSystemState
rs1 = rs0{_r1=1,_r2=2,_r3=3,_r4=4,_r5=5,_r6=6,_r7=7} 
ss1 = ss0 { _ramemory=fromList [0,1,2,3,4,5,6,7,8,9,10],
            _registers=rs1}
aluinstr op v ebm reg = ALUInstr op (Addr (Immediate v)) ebm False reg
testInstr i s = run $ execStateT (interpret i) s
bothGreater = execStateT (interpret $ aluinstr CMP 0 False R6) ss1
signedGreater = execStateT (interpret $ aluinstr CMP (65533) False R6) ss1
equal = execStateT (interpret $ aluinstr CMP 6 False R6) ss1
bothLess = execStateT (interpret $ aluinstr CMP 6 False R0) ss1
rs2 = rs1 { _r0=(65533) }
ss2 = ss1 { _registers = rs2 }
signedLess = execStateT (interpret $ aluinstr CMP 6 False R0) ss2

braTests = do
  describe "Pre-formed BRA tests" $ parallel $ do
    testBUGPre
    testBEQPre
  describe "QuickCheckified BRA tests" $
    it "Branches appropriately (tests all instructions per cycle)" $
      property testQuick

testBUGPre = describe "Testing BUG for bugs :'D" $ do
  it "BUGs after CMP 6 0" $
    (bothGreater >>= execStateT (interpret $ BRAInstr BUG 6)) `shouldReturn`
      (ss1 { _registers = rs1 { _fl = 40960, _pc = 6 } })
  it "Does not BUG after CMP 6 -3" $
    (signedGreater >>= execStateT (interpret $ BRAInstr BUG 6)) `shouldReturn`
      (ss1 { _registers = rs1 { _fl = 32768, _pc = 0 } })
  it "Does not BUG after CMP 6 6" $
    (equal >>= execStateT (interpret $ BRAInstr BUG 6)) `shouldReturn`
      (ss1 { _registers = rs1 { _fl = 57344, _pc = 0 } })
  it "Does not BUG after CMP 0 6" $
    (bothLess >>= execStateT (interpret $ BRAInstr BUG 6)) `shouldReturn`
      (ss1 { _registers = rs1 { _fl = 0, _pc = 0 } })
  it "BUGs after CMP -3 6" $
    (signedLess >>= execStateT (interpret $ BRAInstr BUG 6)) `shouldReturn`
      (ss2 { _registers = rs2 { _fl = 8192, _pc = 6 } })
      
testBEQPre = describe "BEQ" $ do
  it "Does not BEQ after CMP 6 0" $
    (bothGreater >>= execStateT (interpret $ BRAInstr BEQ 6)) `shouldReturn`
      (ss1 { _registers = rs1 { _fl = 40960, _pc = 0 } })
  it "Does not BEQ after CMP 6 -3" $
    (signedGreater >>= execStateT (interpret $ BRAInstr BEQ 6)) `shouldReturn`
      (ss1 { _registers = rs1 { _fl = 32768, _pc = 0 } })
  it "Does BEQ after CMP 6 6" $
    (equal >>= execStateT (interpret $ BRAInstr BEQ 6)) `shouldReturn`
      (ss1 { _registers = rs1 { _fl = 57344, _pc = 6 } })
  it "Does not BEQ after CMP 0 6" $
    (bothLess >>= execStateT (interpret $ BRAInstr BEQ 6)) `shouldReturn`
      (ss1 { _registers = rs1 { _fl = 0, _pc = 0 } })
  it "Does not BEQ after CMP -3 6" $
    (signedLess >>= execStateT (interpret $ BRAInstr BEQ 6)) `shouldReturn`
      (ss2 { _registers = rs2 { _fl = 8192, _pc = 0 } })
      
testQuick op1 op2 = monadicIO $ do
  let sstest = ss0 { _registers = (rs0 & mapGP R0 .~ op1) }
  state <- testInstr (aluinstr CMP op2 False R0) sstest
  bcc <- testInstr (BRAInstr BCCBUF 6) state
  bcs <- testInstr (BRAInstr BCSBUH 6) state
  bne <- testInstr (BRAInstr BNE 6)    state
  beq <- testInstr (BRAInstr BEQ 6)    state
  bpl <- testInstr (BRAInstr BPLBSF 6) state
  bmi <- testInstr (BRAInstr BMIBSH 6) state
  bvc <- testInstr (BRAInstr BVC 6)    state
  bvs <- testInstr (BRAInstr BVS 6)    state
  bug <- testInstr (BRAInstr BUG 6)    state
  bsg <- testInstr (BRAInstr BSG 6)    state
  baw <- testInstr (BRAInstr BAW 6)    state
  assert $ bcc ^. registers . pc == if (op1 < op2) then 6 else 0
  assert $ bcs ^. registers . pc == if (op1 >= op2) then 6 else 0
  assert $ bne ^. registers . pc == if (op1 /= op2) then 6 else 0
  assert $ beq ^. registers . pc == if (op1 == op2) then 6 else 0
  assert $ bpl ^. registers . pc == if ((fromIntegral op1 :: Int16) < fromIntegral op2)
                                     then 6 else 0
  assert $ bmi ^. registers . pc == if ((fromIntegral op1 :: Int16) >= fromIntegral op2)
                                     then 6 else 0
  assert $ bvc ^. registers . pc == if not(testFLV state) then 6 else 0
  assert $ bvs ^. registers . pc == if testFLV state    then 6 else 0
  assert $ bug ^. registers . pc == if (op1 > op2) then 6 else 0
  assert $ bsg ^. registers . pc == if ((fromIntegral op1 :: Int16) > fromIntegral op2)
                                     then 6 else 0
  assert $ baw ^. registers . pc == 6
